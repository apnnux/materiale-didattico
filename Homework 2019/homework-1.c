/*
    Homework n.1

    Scrivere un programma in linguaggio C che permetta di copiare un numero
    arbitrario di file regolari su una directory di destinazione preesistente.

    Il programma dovra' accettare una sintassi del tipo:
     $ homework-1 file1.txt path/file2.txt "nome con spazi.pdf" directory-destinazione
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libgen.h>

#define BUFFER_SIZE 1024

char* getDest(char* input, char* destfolder){
    char* namefile = basename(input);
    char* dest = (char*) malloc(BUFFER_SIZE);
    strcpy(dest, destfolder);
    if(dest[strlen(dest)-1] != '/')
        dest = strcat(dest, "/");

    dest = strcat(dest, namefile);

    return dest;
}

int main(int argc, char* argv[]){

    if(argc < 3){
        printf("USE: homework-1 [file1] [file2...] [dest]\n");
        exit(1);
    }


    FILE* output;
    FILE* input;
    char* dest;

    for(int i = 1; i < argc - 1; i++){
        dest = getDest(argv[i], argv[argc - 1]);

        input = fopen(argv[i], "r");
        output = fopen(dest, "w+");

        char tmp;
        while((tmp = fgetc(input)) != EOF){
            fputc(tmp, output);
        }

        fclose(input);
        fclose(output);
        free(dest);
    }

    printf("COMPLETED :)\n");
}
