/*
    Homework n.3

    Usando la possibilita' di mappare file in memoria, creare un programma che
    possa manipolare un file arbitrariamente grande costituito da una sequenza
    di record lunghi N byte.
    La manipolazione consiste nel riordinare, tramite un algoritmo di ordinamento
    a scelta, i record considerando il contenuto dello stesso come chiave:
    ovvero, supponendo N=5, il record [4a a4 91 f0 01] precede [4a ff 10 01 a3].
    La sintassi da supportare e' la seguente:
     $ homework-3 <N> <pathname del file>

    E' possibile testare il programma sul file 'esempio.txt' prodotto dal seguente
    comando, utilizzando il parametro N=33:
     $ ( for I in `seq 1000`; do echo $I | md5sum | cut -d' ' -f1 ; done ) > esempio.txt

    Su tale file, l'output atteso e' il seguente:
     $ homework-3 33 esempio.txt
     $ head -n5 esempio.txt
        000b64c5d808b7ae98718d6a191325b7
        0116a06b764c420b8464f2068f2441c8
        015b269d0f41db606bd2c724fb66545a
        01b2f7c1a89cfe5fe8c89fa0771f0fde
        01cdb6561bfb2fa34e4f870c90589125
     $ tail -n5 esempio.txt
        ff7345a22bc3605271ba122677d31cae
        ff7f2c85af133d62c53b36a83edf0fd5
        ffbee273c7bb76bb2d279aa9f36a43c5
        ffbfc1313c9c855a32f98d7c4374aabd
        ffd7e3b3836978b43da5378055843c67
*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>


void Sort(char* vett, int size, int N){
   char tmp1[N];
   char tmp2[N];

   for(int i = 0; i < size; i+=N){     //Fuck complexity :)
      for(int j = i+N; j < size; j+=N){
         memcpy(tmp1, &vett[i], N);
         memcpy(tmp2, &vett[j], N);

         if(strcmp(tmp1, tmp2) > 0){
            memcpy(&vett[i], tmp2, N);
            memcpy(&vett[j], tmp1, N);
         }
      }
   }

}

int main (int argc, char* argv[]){
   if(argc < 3){
      printf("USE: %s <size> <input>\n", argv[0]);
      exit(1);
   }

  long fd;
  struct stat st;

  if((fd = open(argv[2], O_RDWR)) == -1){
     perror(argv[2]);
     exit(1);
  }

  if((fstat(fd, &st)) == -1){
     perror("fstat");
  }


  if(!S_ISREG(st.st_mode)){
     fprintf(stderr, "Il file %s non è regolare\n", argv[2]);
     exit(1);
  }

   char* p;

   if((p = (char*) mmap(NULL, st.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)) == MAP_FAILED){
      perror("mmap");
      exit(1);
   }

   if(close(fd) == -1){
      perror("close");
      exit(1);
   }
   
   Sort(p, st.st_size, atoi(argv[1]));
   
   if(munmap(p, st.st_size) == -1){
      perror("munmap");
      exit(1);
   }
}
