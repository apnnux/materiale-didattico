// 7 ottobre 2016 16:08 fine 17:28
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

#define MAX_SIZE 2048
typedef struct{

    long type;
    char path[MAX_SIZE];
    long dim;

} messaggio;

int Analyzer(int msgid){

    messaggio miomessaggio,miomessaggio2;
    struct stat info;
    while(1){

        if(msgrcv(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),1,0) == -1){
            perror("msgrcv");
            return -1;
        }
        if(strcmp(miomessaggio.path,"-1") == 0){
            memset(&miomessaggio2,0,sizeof(miomessaggio2));
            miomessaggio2.type = 2;
            strcpy(miomessaggio2.path,"-1");
            miomessaggio2.dim = -1;
            if(msgsnd(msgid,&miomessaggio2,sizeof(miomessaggio2)-sizeof(long),0) == -1){
                perror("msgsnd");
                return -1;
            }
            break;
        }



        //conteggio
        long calcolato = 0l;
        int fileds;
        if(stat(miomessaggio.path,&info) == -1){
            perror("stat");
            return -1;
        }
        char *areaMemoria;
        if((fileds = open(miomessaggio.path,O_RDONLY)) == -1 ){
            perror("open");
            return -1;
        }
        if((areaMemoria = mmap(NULL,info.st_size,PROT_READ,MAP_PRIVATE,fileds,0)) == (char *)-1){
            perror("mmap");
            return -1;
        }
        for(int i=0;i<info.st_size;i++){
            if(areaMemoria[i]>='a' && areaMemoria[i]<='z')  calcolato++;
            if(areaMemoria[i]>='A' && areaMemoria[i]<='Z')  calcolato++;

        }
        if(munmap(areaMemoria,info.st_size) == -1){
            perror("munmap");
            return -1;
        }
        close(fileds);



        printf("Analyzer: %s %ld\n",miomessaggio.path,calcolato);
        memset(&miomessaggio2,0,sizeof(miomessaggio2));
        miomessaggio2.type = 2;
        strcpy(miomessaggio2.path,miomessaggio.path);
        miomessaggio2.dim = calcolato;
        if(msgsnd(msgid,&miomessaggio2,sizeof(miomessaggio2)-sizeof(long),0) == -1){
            perror("msgsnd");
            return -1;
        }


    }
    return 0;
}

int Scanner(char *cartella, char *directoryprincipale, int msgid){

    struct dirent *buffer;
    DIR *directory;
    messaggio miomessaggio;

    if((directory = opendir(cartella)) == NULL){
        perror("opendir");
        return -1;

    }

    while((buffer = readdir(directory)) != NULL){
        struct stat info2;
        char supporto[MAX_SIZE];

        memset(supporto,0,sizeof(supporto));
        memset(&info2,0,sizeof(info2));

        if(strcmp(buffer->d_name,"..") == 0 || strcmp(buffer->d_name,".") == 0)
            continue;
        strcpy(supporto,cartella);
        strcat(supporto,"/");
        strcat(supporto,buffer->d_name);

        printf("Scanner: %s\n",supporto);

        if(stat(supporto,&info2) == -1){
            perror("stat");
            return -1;
        }

        if((info2.st_mode & S_IFMT) == S_IFREG){

            memset(&miomessaggio,0,sizeof(miomessaggio));
            miomessaggio.type = 1;
            strcpy(miomessaggio.path,supporto);
            miomessaggio.dim = -1;
            if(msgsnd(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),0) == -1){
                perror("msgsnd");
                return -1;
            }
            usleep(5000);
        }

        if((info2.st_mode & S_IFMT) == S_IFDIR){
            Scanner(supporto,cartella,msgid);
        }

    }

    if(strcmp(cartella,directoryprincipale) == 0){
        memset(&miomessaggio,0,sizeof(miomessaggio));
        miomessaggio.type = 1;
        strcpy(miomessaggio.path,"-1");
        miomessaggio.dim = -1;
        if(msgsnd(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),0) == -1){
            perror("msgsnd");
            return -1;
        }
    }

    closedir(directory);

    return 0;
}

int Father( int msgid){

    messaggio miomessaggio;
    long totale =0;
    while(1){

        if(msgrcv(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),2,0) == -1){
            perror("msgrcv");
            return -1;
        }
        if(strcmp(miomessaggio.path,"-1") == 0){
            break;
        }

        totale += miomessaggio.dim;

    }

    printf("Padre: totale di %ld caratteri alfabetici\n",totale);
    return 0;
}


int main(int argc, char* argv[]){

    int msgid;
    char cartella[MAX_SIZE];

    if(argc > 2){
        printf("Use %s [directory]\n",argv[0]);
        return -1;
    }

    if(argc == 1){
        getcwd(cartella,sizeof(cartella));
    }else{
        strcpy(cartella,argv[1]);
    }

    if((msgid = msgget(IPC_PRIVATE,IPC_CREAT|IPC_EXCL|0660)) == -1){
        perror("msgget");
        return -1;
    }

    if( fork() == 0){
        //Scanner
        int return_state = Scanner(cartella,cartella,msgid);
        return return_state;
    }else{

        if(fork() == 0){
            //Analyzer
            int return_state = Analyzer(msgid);
            return return_state;
        }else{
            //Father
            int return_state = Father(msgid);

        }

    }
    wait(NULL);
    wait(NULL);

    if(msgctl(msgid,IPC_RMID,NULL) == -1){
        perror("msgctl");
        return -1;
    }

    return 0;
}
