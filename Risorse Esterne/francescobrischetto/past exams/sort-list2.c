#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <sys/types.h>


#define MAX_SIZE 255
#define NUM_ELEM 1000

typedef struct {
    long type;
    char string1[MAX_SIZE];
    char string2[MAX_SIZE];
    int confr;

} message;


int sorter(int msgfd, int fd, int *pipefd){


    close(pipefd[0]);
    char carattere_letto[1];
    int num,i=0;
    char stringa[MAX_SIZE];

    char parole[NUM_ELEM][MAX_SIZE];
    int cont=0;

    int scambi=0;
    message messaggio;
    //leggo le parole e le carico nel vettore di stringhe parole
    do{
        if((num = read(fd,carattere_letto,sizeof(carattere_letto)))==-1){
            perror("read");
            return -1;
        }
        if(num==0) break;
        if(carattere_letto[0] == '\n'){
            stringa[i] = '\0';
            strcpy(parole[cont],stringa);
            memset(stringa,0,sizeof(stringa));
            i=0;
            cont++;
        }else{
            stringa[i] = carattere_letto[0];
            i++;
        }

    }while(num!=0);


    //bubble sort
    do{
        scambi=0;
        for(int i=0;i<NUM_ELEM-1;i++){
            memset(&messaggio,0,sizeof(messaggio));
            messaggio.type=1;
            strcpy(messaggio.string1,parole[i]);
            strcpy(messaggio.string2,parole[i+1]);
            messaggio.confr=0;
            if((msgsnd(msgfd,&messaggio,sizeof(messaggio)-sizeof(long),0))== -1){
                perror("msgsnd");
                return -1;
            }
            if((msgrcv(msgfd,&messaggio,sizeof(messaggio)-sizeof(long),1,0))== -1){
                perror("msgsnd");
                return -1;
            }

            if(messaggio.confr==1){
                char appoggio[MAX_SIZE];
                strcpy(appoggio,parole[i]);
                strcpy(parole[i],parole[i+1]);
                strcpy(parole[i+1],appoggio);
                scambi++;
            }
        }

    }while(scambi!=0);

    //mando il messaggio di file al comparer
    messaggio.type=1;
    strcpy(messaggio.string1,"0");
    strcpy(messaggio.string2,"0");
    messaggio.confr=0;
    if((msgsnd(msgfd,&messaggio,sizeof(messaggio)-sizeof(long),0))== -1){
        perror("msgsnd");
        return -1;
    }
    //mando tutto al padre nella pipe
    for(int i=0;i<NUM_ELEM;i++){
        if((write(pipefd[1],parole[i],strlen(parole[i])+1))==-1){
            perror("write");
            return -1;
        }
    }
    
    close(fd);

    return 0;
}

int comparer(int msgfd){

    message messaggio;
    message responsemessaggio;
    while(1){
        memset(&responsemessaggio,0,sizeof(responsemessaggio));
        if((msgrcv(msgfd,&messaggio,sizeof(messaggio)-sizeof(long),1,0))== -1){
            perror("msgsnd");
            return -1;
        }
        if(strcmp(messaggio.string1,"0")==0 && strcmp(messaggio.string2,"0")==0){
            break;
        }
        responsemessaggio.type=1;
        strcpy(responsemessaggio.string1,messaggio.string1);
        strcpy(responsemessaggio.string2,messaggio.string2);
        if(strcmp(messaggio.string1,messaggio.string2)>0){
            responsemessaggio.confr=1;
        }else{
            responsemessaggio.confr=0;
        }
        if((msgsnd(msgfd,&responsemessaggio,sizeof(responsemessaggio)-sizeof(long),0))== -1){
            perror("msgsnd");
            return -1;
        }

    }

    return 0;
}

int parent(int *pipefd){

    close(pipefd[1]);
    char carattere_letto[1];
    int i=0,cont=0;
    char stringa[MAX_SIZE];

    do{
        if((read(pipefd[0],carattere_letto,sizeof(carattere_letto)))==-1){
                perror("read");
                return -1;
        }
        if(carattere_letto[0] == '\0'){
                stringa[i] = '\0';
                printf(" %s\n",stringa);
                memset(stringa,0,sizeof(stringa));
                i=0;
                cont++;
        }else{
                stringa[i] = carattere_letto[0];
                i++;
        }

    }while(cont<NUM_ELEM);

    return 0;
}

int main(int argc, char* argv[]){

    int pipefd[2];
    int msgfd;
    int fd;

    if(argc!=2){
        printf("Use %s <file>\n",argv[0]);
        return -1;
    }

    if((fd = open(argv[1],O_RDONLY))==-1){
        perror("open");
        return -1;
    }

    //creo la pipe
    if(pipe(pipefd)==-1){
        perror("pipe");
        return -1;
    }

    //creo la coda di messaggi
    if((msgfd = msgget(IPC_PRIVATE,IPC_CREAT|IPC_EXCL|0660))==-1){
        perror("msgget");
        return -1;
    }

    if(fork() == 0){
        //Sorter
        int return_value = sorter(msgfd,fd,pipefd);
        return return_value;

    }else{

        if(fork() == 0){
            //Comparer
            int return_value = comparer(msgfd);
            return return_value;

        }else{
            //Padre
            int return_value = parent(pipefd);

            //aspetto i figli
            wait(NULL);
            wait(NULL);
            //cancello la coda di messaggi
            if((msgctl(msgfd,IPC_RMID,NULL))== -1){
                perror("msgctl");
                return -1;
            }
	    close(fd);
            return return_value;
        }
    }




    return 0;
}
