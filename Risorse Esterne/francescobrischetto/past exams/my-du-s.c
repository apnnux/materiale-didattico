//20 luglio 2017

#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>

#define MUTEXSTSC 0
#define FULLSTSC  1
#define EMPTYSTSC 2
#define MUTEXSTP  3
#define FULLSTP   4
#define EMPTYSTP  5

#define MAX_PATH_LEN 255


typedef struct {

    char path[MAX_PATH_LEN];
    int numScanner;
    int numBlocks;

} messaggio;

int WAIT(int sem_des, int num_semaforo){
    struct sembuf operazioni[1] = {{num_semaforo,-1,0}};
    return semop(sem_des,operazioni,1);
}

int SIGNAL(int sem_des, int num_semaforo){
    struct sembuf operazioni[1] = {{num_semaforo,+1,0}};
    return semop(sem_des,operazioni,1);
}

int Stater(int semds,messaggio *msgStSc, messaggio *msgStP, int numScan){

    //printf("St: Stater Creato!\n");

    int verificato =0;
    int conteggio=0;
    while(verificato == 0){

        messaggio supporto;
        memset(&supporto,0,sizeof(supporto));


        WAIT(semds,EMPTYSTP);
        WAIT(semds,MUTEXSTP);

        WAIT(semds,FULLSTSC);
        WAIT(semds,MUTEXSTSC);

        strcpy(supporto.path,msgStSc->path);
        supporto.numBlocks = msgStSc->numBlocks;
        supporto.numScanner = msgStSc->numScanner;

        if(strcmp(supporto.path,"-1") !=0){
            struct stat statbuffer;

            if((lstat(supporto.path,&statbuffer))==-1){
            perror("lstat");
            return -1;
            }
            supporto.numBlocks = statbuffer.st_blocks;

        }else{
            conteggio++;
            if(conteggio == numScan){
                verificato = 1;
                supporto.numBlocks =-1;
                strcpy(supporto.path,"-1");
            }else{
                supporto.numBlocks =0;
                strcpy(supporto.path,"1");
            }
        }

        strcpy(msgStP->path,supporto.path);
        //printf("%s %d\n",msgStP->path,supporto.numBlocks);
        msgStP->numBlocks = supporto.numBlocks;
        msgStP->numScanner = supporto.numScanner;

        SIGNAL(semds,MUTEXSTSC);
        SIGNAL(semds,EMPTYSTSC);

        SIGNAL(semds,MUTEXSTP);
        SIGNAL(semds,FULLSTP);




    }

    return 0;
}

int Scanner2(int semds,messaggio *msgStSc,int numScan,char *path){

    //printf("Sc: Scanner %d Creato!\n",numScan);
    struct stat statbuffer;
    DIR *directory;
    struct dirent *elementoLetto;
    int finito = 0;

    if((lstat(path,&statbuffer))==-1){
        perror("lstat");
        return -1;
    }

    if(!S_ISDIR(statbuffer.st_mode)){
        printf("Errore, %s non e' una directory\n",path);
        //avvisa Stater

        WAIT(semds,EMPTYSTSC);
        WAIT(semds,MUTEXSTSC);

        msgStSc->numBlocks=-1;
        msgStSc->numScanner=numScan;
        strcpy(msgStSc->path,"-1");

        SIGNAL(semds,MUTEXSTSC);
        SIGNAL(semds,FULLSTSC);

        return -1;
    }

    if((directory = opendir(path)) == NULL){
        perror("opendir");
        return -1;
    }

    while(finito == 0){
        elementoLetto = readdir(directory);
        if((elementoLetto) == NULL){
            finito = 1;
        }else{

            if(strcmp(elementoLetto->d_name,".") == 0 || strcmp(elementoLetto->d_name,"..") == 0){
                continue;
            }else{

                struct stat statbuffer2;

                char pathfile[MAX_PATH_LEN];
                memset(pathfile,0,sizeof(pathfile));
                strcpy(pathfile,path);
                strcat(pathfile,"/");
                strcat(pathfile,elementoLetto->d_name);

                memset(&statbuffer2,0,sizeof(statbuffer2));
                if((lstat(pathfile,&statbuffer2))==-1){
                    perror("lstat");
                    return -1;
                }

                if(S_ISDIR(statbuffer2.st_mode)){
                    Scanner2(semds,msgStSc,numScan,pathfile);
                }else{
                    //devo mandare il messaggio
                    WAIT(semds,EMPTYSTSC);
                    WAIT(semds,MUTEXSTSC);

                    msgStSc->numBlocks=-1;
                    msgStSc->numScanner=numScan;
                    strcpy(msgStSc->path,pathfile);

                    SIGNAL(semds,MUTEXSTSC);
                    SIGNAL(semds,FULLSTSC);

                }
            }

        }

    }


    if((closedir(directory))== -1){
        perror("closedir");
        return -1;
    }
    return 0;
}

int Scanner(int semds,messaggio *msgStSc,int numScan,char *path){

    //printf("Sc: Scanner %d Creato!\n",numScan);
    struct stat statbuffer;
    DIR *directory;
    struct dirent *elementoLetto;
    int finito = 0;

    if((lstat(path,&statbuffer))==-1){
        perror("lstat");
        return -1;
    }

    if(!S_ISDIR(statbuffer.st_mode)){
        printf("Errore, %s non e' una directory\n",path);
        //avvisa Stater

        WAIT(semds,EMPTYSTSC);
        WAIT(semds,MUTEXSTSC);

        msgStSc->numBlocks=-1;
        msgStSc->numScanner=numScan;
        strcpy(msgStSc->path,"-1");

        SIGNAL(semds,MUTEXSTSC);
        SIGNAL(semds,FULLSTSC);

        return -1;
    }

    if((directory = opendir(path)) == NULL){
        perror("opendir");
        return -1;
    }

    while(finito == 0){
        elementoLetto = readdir(directory);
        if((elementoLetto) == NULL){
            finito = 1;
        }else{

            if(strcmp(elementoLetto->d_name,".") == 0 || strcmp(elementoLetto->d_name,"..") == 0){
                continue;
            }else{

                struct stat statbuffer2;

                char pathfile[MAX_PATH_LEN];
                memset(pathfile,0,sizeof(pathfile));
                strcpy(pathfile,path);
                strcat(pathfile,"/");
                strcat(pathfile,elementoLetto->d_name);

                memset(&statbuffer2,0,sizeof(statbuffer2));
                if((lstat(pathfile,&statbuffer2))==-1){
                    perror("lstat");
                    return -1;
                }

                if(S_ISDIR(statbuffer2.st_mode)){
                    Scanner2(semds,msgStSc,numScan,pathfile);
                }else{
                    //devo mandare il messaggio
                    WAIT(semds,EMPTYSTSC);
                    WAIT(semds,MUTEXSTSC);

                    msgStSc->numBlocks=-1;
                    msgStSc->numScanner=numScan;
                    strcpy(msgStSc->path,pathfile);

                    SIGNAL(semds,MUTEXSTSC);
                    SIGNAL(semds,FULLSTSC);

                }
            }

        }

    }

    WAIT(semds,EMPTYSTSC);
    WAIT(semds,MUTEXSTSC);

    msgStSc->numBlocks=-1;
    msgStSc->numScanner=numScan;
    strcpy(msgStSc->path,"-1");

    SIGNAL(semds,MUTEXSTSC);
    SIGNAL(semds,FULLSTSC);

    if((closedir(directory))== -1){
        perror("closedir");
        return -1;
    }


    return 0;
}


int Padre(int semds,messaggio *msgStP,int argc,char *argv[]){

    //printf("P: Padre Creato!\n");
    int verificato =0;
    int contatori[argc];
    for(int i=0;i<argc;i++){
        contatori[i] =0;
    }


    while(verificato == 0){

        WAIT(semds,FULLSTP);
        WAIT(semds,MUTEXSTP);

        if(strcmp(msgStP->path,"-1")==0){
            verificato =1;
        }else{
            contatori[msgStP->numScanner]+=msgStP->numBlocks;
        }

        SIGNAL(semds,MUTEXSTP);
        SIGNAL(semds,EMPTYSTP);
    }

    for(int i=1;i<argc;i++){
        printf("%d\t %s\n",contatori[i]/2 +4,argv[i]);
    }

    return 0;
}

int main(int argc, char* argv[]){

    int shmds1,shmds2;
    int semds;
    messaggio *msgStSc, *msgStP;

    if(argc<2){
        printf("Use %s [path1] ... [pathN]\n",argv[0]);
        return -1;
    }

    if((shmds1 = shmget(IPC_PRIVATE,sizeof(messaggio),IPC_CREAT|IPC_EXCL|0660))== -1){
        perror("shmget");
        return -1;
    }
    if((shmds2 = shmget(IPC_PRIVATE,sizeof(messaggio),IPC_CREAT|IPC_EXCL|0660))== -1){
        perror("shmget");
        return -1;
    }

    if((msgStSc = (messaggio *)shmat(shmds1,NULL,0))== (messaggio *)-1){
        perror("shmat");
        return -1;
    }
    if((msgStP = (messaggio *)shmat(shmds2,NULL,0))== (messaggio *)-1){
        perror("shmat");
        return -1;
    }


    if((semds = semget(IPC_PRIVATE,6,IPC_CREAT|IPC_EXCL|0660))== -1){
        perror("semget");
        return -1;
    }

    semctl(semds,MUTEXSTSC,SETVAL,1);
    semctl(semds,EMPTYSTSC,SETVAL,1);
    semctl(semds,FULLSTSC, SETVAL,0);
    semctl(semds,MUTEXSTP, SETVAL,1);
    semctl(semds,EMPTYSTP, SETVAL,1);
    semctl(semds,FULLSTP,  SETVAL,0);


    if(fork() == 0){
        //Stater
        int return_value = Stater(semds,msgStSc,msgStP,argc-1);
        return return_value;

    }else{
        for(int i=1;i<argc;i++){
            if(fork() == 0){
                //Scanner i
                int return_value = Scanner(semds,msgStSc,i,argv[i]);
                return return_value;
            }
        }
        //Padre

        int return_value = Padre(semds,msgStP,argc,argv);

        for(int i=1;i<argc;i++){
            wait(NULL);
        }

        wait(NULL);

        semctl(semds,MUTEXSTSC,IPC_RMID,0);
        semctl(semds,FULLSTSC,IPC_RMID,0);
        semctl(semds,EMPTYSTSC,IPC_RMID,0);
        semctl(semds,MUTEXSTP,IPC_RMID,0);
        semctl(semds,FULLSTP,IPC_RMID,0);
        semctl(semds,EMPTYSTP,IPC_RMID,0);

        if((shmctl(shmds1,IPC_RMID,NULL))== -1){
            perror("shmctl");
            return -1;
        }
        if((shmctl(shmds2,IPC_RMID,NULL))== -1){
            perror("shmctl");
            return -1;
        }

        return return_value;

    }








    return 0;

}
