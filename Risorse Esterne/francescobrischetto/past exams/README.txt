La prima riga del codice contiene un commento relativo alla data dell'esame di riferimento.
Potrebbe anche contenere degli orari, rispettivamente di inizio e fine svolgimento che mi sono stati utili per cronometrarmi 
nell'esecuzione della prova.
Alcuni esami, specialmente i primi che ho svolto, sono inefficienti e alcuni punti del codice andrebbero rivisitati.
Sono tutti perfettamente funzionanti, ma molti hanno bisogno dei file di supporto specificati dal professore e qui non riportati.

										