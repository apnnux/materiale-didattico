//13 luglio 2016
#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAX_SIZE 255

int Reader(int pipeid[2],char *file){

    char buffer[MAX_SIZE];
    if( strcmp(file, "stdin") == 0){
        do{
            memset(&buffer,0,sizeof(buffer));
            printf("> ");
            fgets(buffer,sizeof(buffer),stdin);
            buffer[strlen(buffer)-1] = '\0';
            if(write(pipeid[1],buffer,sizeof(buffer)) == -1){
                perror("write");
                return -1;
            }

        }while(strcmp(buffer,"!quit")!=0);


    }else{
        FILE *fileds;

        if((fileds = fopen(file,"r")) == NULL){
            perror("open");
            if(write(pipeid[1],"!quit",strlen("!quit")) == -1){
                perror("write");
                return -1;
            }
            return -1;
        }

        while(feof(fileds)!=1){
            memset(&buffer,0,sizeof(buffer));
            fgets(buffer,sizeof(buffer),fileds);
            buffer[strlen(buffer)-1] = '\0';
            if(strcmp(buffer,"") == 0) continue;
            if(write(pipeid[1],buffer,sizeof(buffer)) == -1){
                perror("write");
                return -1;
            }
        }
        if(write(pipeid[1],"!quit",strlen("!quit")) == -1){
                perror("write");
                return -1;
        }

        fclose(fileds);


    }

    return 0;
}

int Filterer(int pipeid[2],char *word,int commandI,int commandV, char* path_fifo){

    char buffer[MAX_SIZE];
    int fifoid;
    do{
        memset(buffer,0,sizeof(buffer));
        if(read(pipeid[0],buffer,sizeof(buffer)) == -1){
                perror("read");
                return -1;
        }

        if((fifoid=open(path_fifo,O_WRONLY)) == -1){
            perror("open");
            return -1;
        }

        if(strcmp(buffer,"!quit")==0){
            if(write(fifoid,buffer,sizeof(buffer)) == -1){
                        perror("write");
                        return -1;
            }
            break;
        }
        if(commandI == 1){

            if(commandV == 1){
                if(strcasestr(buffer,word)==NULL){
                    //spedire
                    if(write(fifoid,buffer,sizeof(buffer)) == -1){
                        perror("write");
                        return -1;
                    }
                }

            }else{
                if(strcasestr(buffer,word)!=NULL){
                    //spedire
                    if(write(fifoid,buffer,sizeof(buffer)) == -1){
                        perror("write");
                        return -1;
                    }
                }
            }

        }else{

            if(commandV == 1){
                if(strstr(buffer,word)==NULL){
                    //spedire
                    if(write(fifoid,buffer,sizeof(buffer)) == -1){
                        perror("write");
                        return -1;
                    }
                }

            }else{
                if(strstr(buffer,word)!=NULL){
                    //spedire
                    if(write(fifoid,buffer,sizeof(buffer)) == -1){
                        perror("write");
                        return -1;
                    }
                }
            }

        }


    }while(strcmp(buffer,"!quit")!=0);

    close(fifoid);
    return 0;
}


int Writer(char *path_fifo){

    char buffer[MAX_SIZE];
    int fifoid;
    if((fifoid=open(path_fifo,O_RDONLY)) == -1){
            perror("open");
            return -1;
    }

    do{
        memset(buffer,0,sizeof(buffer));
        if(read(fifoid,buffer,sizeof(buffer)) == -1){
            perror("read");
            return -1;
        }
        if(strcmp(buffer,"!quit")==0){
            break;
        }
        printf("%s\n",buffer);

    }while(strcmp(buffer,"!quit")!=0);
    return 0;
}


int main(int argc,char *argv[]){

    int pipeid[2];
    char path_fifo[MAX_SIZE];

    int commandI,commandV;
    commandI = commandV =0;
    int numPar = argc-1;
    char file[MAX_SIZE],word[MAX_SIZE];

    if(argc <2 || argc >5){
        printf("Use %s [-i] [-v] <word> [file]\n",argv[0]);
        return -1;
    }

    else if(argc>2){
        if(strcmp(argv[1],"-i")==0 || (strcmp(argv[1],"-v")==0 && strcmp(argv[2],"-i")==0) ){
            commandI=1; numPar--;
        }
        if(strcmp(argv[1],"-v")==0 || (commandI==1 && strcmp(argv[2],"-v")==0) ){
            commandV=1; numPar--;
        }
    }
    if(numPar == 1)     strcpy(file,"stdin");
    else                strcpy(file,argv[argc-numPar+1]);

    strcpy(word,argv[argc-numPar]);

    getcwd(path_fifo,sizeof(path_fifo));
    strcat(path_fifo,"/");
    strcat(path_fifo,"fifo");

    if((pipe(pipeid)) == -1){
        perror("pipe");
        return -1;
    }

    if((mkfifo(path_fifo,IPC_CREAT|IPC_EXCL|0660)) == -1){
        perror("mkfifo");
        return -1;
    }

    if(fork() == 0){
        //Reader
        close(pipeid[0]);
        int return_state = Reader(pipeid,file);
        return return_state;
    }

    if(fork() == 0){
        //Filterer
        close(pipeid[1]);
        int return_state = Filterer(pipeid,word,commandI,commandV,path_fifo);
        return return_state;
    }

    if(fork() == 0){
        //Writer
        close(pipeid[0]);
        close(pipeid[1]);
        int return_state = Writer(path_fifo);
        return return_state;
    }

    wait(NULL);
    wait(NULL);
    wait(NULL);

    unlink(path_fifo);
    return 0;
}
