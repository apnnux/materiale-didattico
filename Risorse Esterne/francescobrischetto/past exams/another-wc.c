#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MUTEX 0
#define EMPTY 1
#define FULL 2
#define NUM_SEM 3
#define DIM 1025

int WAIT(int sem_des, int num_semaforo){
    struct sembuf operazioni[1] = {{num_semaforo,-1,0}};
    return semop(sem_des,operazioni,1);
}

int SIGNAL(int sem_des, int num_semaforo){
    struct sembuf operazioni[1] = {{num_semaforo,+1,0}};
    return semop(sem_des,operazioni,1);
}


int Figlio(char *memCond,int semid, char *file){
    //printf("Figlio Creato!\n");
    char buffer[DIM-1];

    if(strcmp(file,"stdin") == 0){
        while(1){
            memset(buffer,0,sizeof(buffer));
            fgets(buffer,sizeof(buffer),stdin);
            if(strcmp(buffer,"!quit\n") == 0)   break;
            WAIT(semid,EMPTY);
            WAIT(semid,MUTEX);
            strcpy(memCond,buffer);
            memCond[DIM-1] = 1;
            SIGNAL(semid,MUTEX);
            SIGNAL(semid,FULL);
        }
        WAIT(semid,EMPTY);
        WAIT(semid,MUTEX);
        memCond[DIM-1] = 0;
        SIGNAL(semid,MUTEX);
        SIGNAL(semid,FULL);

    }else{
        int fileds;
        if((fileds = open(file,O_RDONLY)) == -1){
            perror("open");
            return -1;
        }
        while(read(fileds,buffer,sizeof(buffer)) != 0){

            WAIT(semid,EMPTY);
            WAIT(semid,MUTEX);
            strcpy(memCond,buffer);
            memCond[DIM-1] = 1;
            SIGNAL(semid,MUTEX);
            SIGNAL(semid,FULL);

        }

        WAIT(semid,EMPTY);
        WAIT(semid,MUTEX);
        memCond[DIM-1] = 0;
        SIGNAL(semid,MUTEX);
        SIGNAL(semid,FULL);


    }

    return 0;

}

int Padre(char* memCond, int semid,char *file){
    int contC =0;
    int contP=0;
    int contR =0;
    int fine=0;
    while(1){

      WAIT(semid,FULL);
      WAIT(semid,MUTEX);
      if(memCond[DIM-1] == 0)   break;
      for(int i=0;i<strlen(memCond);i++){

        if(memCond[i] == '\n'){ contR++; contP++;}

        if(memCond[i] == '.'||
           memCond[i] == ','||
           memCond[i] == ';'||
           memCond[i] == ':'||
           memCond[i] == '!'||
           memCond[i] == '\t'||
           memCond[i] == ' ')  contP++;

        contC++;
      }

      SIGNAL(semid,MUTEX);
      SIGNAL(semid,EMPTY);

    }

    printf("c: %d p: %d r: %d %s\n",contC,contP,contR,file);

    return 0;
}


int main(int argc, char* argv[]){

    int shmid, semid;
    char *memCond;

    if(argc>2){
        printf("Usa %s [file di testo]\n",argv[0]);
        return -1;
    }

    if((shmid=shmget(IPC_PRIVATE,DIM,IPC_CREAT|IPC_EXCL|0660))==-1){
        perror("shmget");
        return -1;
    }

    if((semid= semget(IPC_PRIVATE, NUM_SEM, IPC_CREAT|IPC_EXCL|0660))==-1){
        perror("semget");
        return -1;
    }

    semctl(semid,MUTEX,SETVAL,1);
    semctl(semid,EMPTY,SETVAL,1);
    semctl(semid,FULL,SETVAL, 0);

    if((memCond = (char*)shmat(shmid,NULL,0)) == NULL){
        perror("shmat");
        return -1;
    }

    if(fork() == 0){
        //Figlio
        int return_state;
        if(argc == 1)
            return_state = Figlio(memCond,semid,"stdin");
        else
            return_state = Figlio(memCond,semid,argv[1]);

        return return_state;

    }else{
        //Padre
        if(argc == 1)
            Padre(memCond,semid,"stdin");
        else
            Padre(memCond,semid,argv[1]);
    }

    wait(NULL);
    semctl(semid,MUTEX,IPC_RMID,0);
    semctl(semid,EMPTY,IPC_RMID,0);
    semctl(semid,FULL ,IPC_RMID,0);

    shmctl(shmid,IPC_RMID,NULL);

    return 0;
}
