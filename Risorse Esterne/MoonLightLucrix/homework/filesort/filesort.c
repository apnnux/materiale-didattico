#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/mman.h>
#include<unistd.h>
#include<errno.h>
#define N 4096
#define true 1
#define false 0
typedef char bool;

void quicksort(char*vett, int sx, int dx, int n)
{
	int i=sx, j=dx;
	char pivot[n], a[n];
	memcpy(pivot,vett+((i+j)>>1)*n,n);
	while(i<=j)
	{
		while(memcmp(pivot,vett+i*n,n)>0)
		{
			i++;
		}
		while(memcmp(pivot,vett+j*n,n)<0)
		{
			j--;
		}
		if(i<=j)
		{
			memcpy(a,vett+i*n,n);
			memcpy(vett+i*n,vett+j*n,n);
			memcpy(vett+j*n,a,n);
			i++;
			j--;
		}
	}
	if(sx<j)
	{
		quicksort(vett,sx,j,n);
	}
	if(i<dx)
	{
		quicksort(vett,i,dx,n);
	}
}

void usage(char**opt)
{
	if(errno==ENOENT)
	{
		printf("%s: '%s' Il file o directory non esiste\n",opt[0],opt[2]);
	}
	else if(errno==EEXIST)
	{
		printf("%s: Il file esiste\n",opt[0]);
	}
	else if(errno==EACCES)
	{
		printf("%s: Non posso scrivere nel file\n",opt[0]);
	}
	else
	{
		printf("usage: %s Bytes_number src_file\n",opt[0]);
	}
}

int main(int argc,char**argv)
{
	extern int errno;
	if(argc!=3)
	{
		usage(argv);
		exit(0);
	}
	char*buffer;
	struct stat statBuffFile;
	int sd, n=atoi(argv[1]);
	if((sd=open(argv[2],O_RDWR))==-1)
	{
		usage(argv);
		exit(1);
	}
	if((fstat(sd,&statBuffFile))==-1)
	{
		perror("error: can't stating file");
		exit(1);
	}
	if((statBuffFile.st_mode & S_IFMT)!=S_IFREG)
	{
		perror("error: It's not a regular file");
		exit(1);
	}
	if((buffer=(char*)mmap(NULL,statBuffFile.st_size,PROT_READ | PROT_WRITE,MAP_SHARED,sd,0))==MAP_FAILED)
	{
		usage(argv);
		exit(1);
	}
	if((close(sd))==-1)
	{
		perror("error: can't close file");
		exit(1);
	}
	quicksort(buffer,0,(statBuffFile.st_size/n)-1,n);
	printf("%s\n",buffer);
	munmap(buffer,statBuffFile.st_size);
	exit(0);
}
