#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<sys/sem.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/mman.h>
#include<unistd.h>
#include<sys/wait.h>
#include<errno.h>
#define true 1
#define false 0
#define N 4096
#define STDIN 0
#define STDOUT 1
#define STDERR 2
#define S_OUTPUT 0
#define S_CONDITION 1
typedef int bool;

int NOTIFY(int semid,int condition) //SIGNAL è uguale
{
    struct sembuf operazioni[1]={{condition,+1,0}};
    return semop(semid,operazioni,1);
}

int WAIT(int semid,int condition)
{
    struct sembuf operazioni[1]={{condition,-1,0}};
    return semop(semid,operazioni,1);
}

int inserisci(int shmid,int semid)
{
    char*messaggio;
    if((messaggio=(char*)shmat(shmid,NULL,IPC_NOWAIT))==(char*)-1)
    {
        perror("error: shmat");
        return -1;
    }
    size_t length;
    while(true)
    {
        printf("inserisci comando ");
        fgets(messaggio,N,stdin);
        if((length=strlen(messaggio))==1)
        {
            continue;
        }
        else if(messaggio[length-1]=='\n')
        {
            messaggio[length-1]='\0';
        }
        NOTIFY(semid,S_CONDITION);
        if(!strcmp(messaggio,"exit"))
        {
            break;
        }
        WAIT(semid,S_OUTPUT);
        printf("messaggio: \"%s\"\n",messaggio);
    }
    return 0;
}

int esegui(int shmid,int semid)
{
    char*messaggio;
    if((messaggio=(char*)shmat(shmid,NULL,IPC_NOWAIT))==(char*)-1)
    {
        perror("error: shmat");
        return -1;
    }
    int pipefd[2];
    while(true)
    {
        WAIT(semid,S_CONDITION);
        if(!strcmp(messaggio,"exit"))
        {
            break;
        }
        if((pipe(pipefd))==-1)
        {
            perror("error: pipe");
            return -1;
        }
        pid_t pid;
        if((pid=fork())==-1)
        {
            perror("error: can't create a new process");
            return -1;
        }
        if(!pid)
        {
            printf("%s\n",messaggio);
            close(pipefd[0]);
            close(STDOUT);
            dup(pipefd[1]);
            close(STDERR);
            dup(pipefd[1]);
            if((execlp(messaggio,messaggio,NULL))==-1)
            {
                fprintf(stderr,"error: \"%s\" command not found\n",messaggio);
                exit(EXIT_FAILURE);
            }
            exit(EXIT_SUCCESS);
        }
        else
        {
            close(pipefd[1]);
            ssize_t size;
            if((size=read(pipefd[0],messaggio,N))==-1)
            {
                perror("error: can't read file");
                return -1;
            }
            messaggio[size-1]='\0';
            NOTIFY(semid,S_OUTPUT);
            close(pipefd[0]);
        }
    }
    shmctl(shmid,IPC_RMID,NULL);
    semctl(semid,0,IPC_RMID,0);
    return 0;
}

int msgShareInit(char*program)
{
    int shmid;
    int semid;
    mode_t mode=S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    key_t chiave=IPC_PRIVATE;
    if((shmid=shmget(chiave,N,IPC_CREAT | IPC_EXCL | mode))==-1)
    {
        perror("error: shmget");
        return -1;
    }
    if((semid=semget(chiave,2,IPC_CREAT | IPC_EXCL | mode))==-1)
    {
        perror("error: semget");
        return -1;
    }
    semctl(semid,S_OUTPUT,SETVAL,0);
    semctl(semid,S_CONDITION,SETVAL,0);
    pid_t pid;
    if((pid=fork())==-1)
    {
        perror("error: can't create a new process");
        return -1;
    }
    if(!pid)
    {
        if((esegui(shmid,semid))==-1)
        {
            perror("error: esegui");
            exit(EXIT_FAILURE);
        }
        exit(EXIT_SUCCESS);
    }
    else
    {
        if((inserisci(shmid,semid))==-1)
        {
            perror("error: inserisci");
            return -1;
        }
    }
    return 0;
}

int main(int argc,char**argv)
{
    if(msgShareInit(argv[0])==-1)
    {
        perror("error: can't start program");
        exit(EXIT_FAILURE);
    }
    exit(EXIT_SUCCESS);
}
